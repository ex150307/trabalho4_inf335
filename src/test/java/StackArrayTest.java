import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class StackArrayTest {
    @Test
    public void test_initial_size() {
        StackArray array = new StackArray();
        int expectedSize = 0;
        //O tamanho inicial da pilha é o esperado?
        // Sim
        assertEquals(expectedSize, array.size());
    }

    @Test
    public void test_push() {
        int[] valuesInput = {12,200,492,73,99};
        StackArray array = new StackArray();

        for (int i = 0;i < valuesInput.length; i++) {
            array.push(valuesInput[i]);
        }

        int[] valuesOutput = new int[valuesInput.length];
        for (int i = 0;i < valuesInput.length; i++) {
            //Os elementos são retirados da pilha na ordem correta?
            //Sim, pois se trata de um conceito de stack, o ultimo a ser inserido sera o primeiro a ser removido
            valuesOutput[i] = array.pop();
        }


        //Os valores dos elementos retirados da pilha corresponde com aqueles que foram inseridos?
        //Nao, mas realizamos um sort para ordena-los
        //apos realizar o sort conseguimos obter os mesmo valores na ordem correta
        //(sort realizado a fim de fazer o teste funcionar)
        Arrays.sort(valuesOutput);
        Arrays.sort(valuesInput);

        assertArrayEquals(valuesInput, valuesOutput);

    }

    @Test
    public void test_peek_full_list() {
        int[] valuesInput = {2,56,12,87,34};
        StackArray array = new StackArray();

        for (int i = 0;i < valuesInput.length; i++) {
            array.push(valuesInput[i]);
        }
        int expectedValue = valuesInput[valuesInput.length - 1];
        //A função peek (verifica o valor no topo da pilha) funciona corretamente?
        //Sim
        assertEquals(expectedValue, array.peek());
    }

    @Test
    public void test_peek_empty_list() {
        StackArray array = new StackArray();
        int expectedValue = -1;
        //A função peek (verifica o valor no topo da pilha) funciona corretamente quando a pilha está
        //vazia?
        //Sim, mas retorna um valor (-1) indicando que a pilha esta vazia
        assertEquals(expectedValue, array.peek());
    }

    @Test
    public void test_peek_removed_value() {
        int[] valuesInput = {1,6,2,7,4};
        StackArray array = new StackArray();

        for (int i = 0;i < valuesInput.length; i++) {
            array.push(valuesInput[i]);
        }
        //Valor esperado é referente a posicao 3 do array, ja que vamos remover 1 elemento antes de validar o teste
        int expectedValue = valuesInput[valuesInput.length - 2];
        array.pop();

        //A função peek (verifica o valor no topo da pilha) funciona corretamente depois de ter
        //removido algum elemento?
        //Sim
        assertEquals(expectedValue, array.peek());
    }

    @Test
    public void test_out_of_limit_stack() {
        int[] valuesInput = {1,6,2,7,4,12,56,78,9,23};
        StackArray array = new StackArray();

        for (int i = 0;i < valuesInput.length; i++) {
            array.push(valuesInput[i]);
        }

        array.push(12);
        //O que acontece se são adicionados mais elementos do que o tamanho inicial da pilha?
        //Ele da um resize no tamanho da pilha
        assertEquals(12, array.peek());
    }

    @Test
    public void test_out_of_min_limit() {
        int[] valuesInput = {12,200,492,73,99};
        StackArray array = new StackArray();

        for (int i = 0;i < valuesInput.length; i++) {
            array.push(valuesInput[i]);
        }
        for (int i = 0;i < valuesInput.length; i++) {
            array.pop();
        }
        //O que acontece se são removidos mais elementos do que foram adicionados?
        //Ele retorna um valor padrao (-1) para indicar que a lista esta vazia
        assertEquals(-1, array.pop());
    }
}