public class BubbleSort {

	public static void sort(int[] vector) {
		boolean switched = true;
		while (switched) {
			switched = false;
			for (int i = 0; i < vector.length + 1; i++) {
				for(int j = 0; j<vector.length - 1; j++){
					sortPositions(vector, j);
				}
			}
		}
	}

	private static void sortPositions(int[] vector, int j) {
		int aux;
		if (vector[j] > vector[j + 1]) {
			aux = vector[j];
			vector[j] = vector[j + 1];
			vector[j +1] = aux;
			}
	}
}

