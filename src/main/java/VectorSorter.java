import org.apache.log4j.Logger;

public class VectorSorter {

	public static final int SIZE_OF_VECTOR = 10;
	public static final int MAX_VECTOR_VALUE = 100;
	public static final int MIN_VECTOR_VALUE = 0;

	static final Logger logger = Logger.getLogger(VectorSorter.class);

	public static void main(String[] args) {
		int[] numbers = parseParameters(args);

		logger.info("Input: ");
		printVector(numbers);
		sort(numbers);
		logger.info("Sorted: ");
		printVector(numbers);
	}
	
	public static void sort(int[] numbers) {
		BubbleSort.sort(numbers);
	}
	
	public static int[] parseParameters(String[] args) {
		int[] numbers;
		if(args.length > 0) {
			numbers = new int[args.length];
			for(int k=0; k<args.length; k++) {
				numbers[k] = Integer.parseInt(args[k]);
			}
		}else {
			numbers = generateRandomVector(SIZE_OF_VECTOR);
		}
		return numbers;
	}
	
	private static int[] generateRandomVector(int size) {

		int[] vector = new int[size];

		for (int i = MIN_VECTOR_VALUE; i < vector.length ; i++) {
			vector[i] = (int) (Math.random()* MAX_VECTOR_VALUE + 1);
		}
		
		return vector;
	}
	
	public static void printVector(int[] numbers) {
		logger.info("[ ");
		logger.info(numbers[0]);

		int i = 0;
		do {
			i++;
			logger.info(", ");
			logger.info(numbers[i]);
		}while(i < numbers.length - 1);

		logger.info(" ] ");
	}
}
